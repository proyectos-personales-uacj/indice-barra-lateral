<?php
$remitente = $_POST['email'];
$destinatario = 'adrian.duran@uacj.mx'; // en esta línea va el mail del destinatario.
$asunto = 'Consulta'; // aquí se puede modificar el asunto del mail
if (!$_POST){
?>
<!DOCTYPE html>
<html>
<head>
	<title>Contacto</title>
    <meta charset="UTF-8">
    <title>Festival de Diseño Gráfico</title>
    <link rel="stylesheet" href="css/app_contacto.css">
</head>
<body>
	<header>
        <nav class="anima">
          <ul>
            <li><a href="index.html">Inicio</a></li>
            <li><a href="programacion.html">¿Cuáles son los objetivos de los ODS?</a></li>
            <li><a href="ponentes.html">¿Cómo pueden los ODS ser alcanzados?</a></li>
            <li><a href="energias.html">Energias renovables</a></li>
            <li><a href="contacto.html">Contacto</a></li>
          </ul>
          </nav>
	</header>
	
	<main>
		<h2>Información de Contacto</h2>
		<p>Por favor contáctanos usando la siguiente información: <br><br></p>
		<ul>
			<li>Teléfono: 555-1234</li><br>
			<li>Correo Electrónico: desing_cn@festivaledg.com</li><br>
			<li>Dirección: Cooper-Hewitt, National Design Museum
                2 East 91st Street
                New York City
                </li><br>
		</ul>
		
		<h2>Formulario de Contacto</h2>
		<form method="POST">
			<label for="nombre">Nombre:</label>
			<input type="text" name="nombre" id="nombre" required>

			<label for="email">Correo Electrónico:</label>
			<input type="email" name="email" id="email">

			<label for="telefono">Teléfono:</label>
			<input type="tel" name="telefono" id="telefono">

			<label for="mensaje">Mensaje:</label>
			<textarea name="mensaje" id="mensaje"></textarea>

			<!--<button type="submit" value="Enviar">Enviar</button>-->
      <input type="button" value="Enviar" onclick="sendEmail()">
			<button type="reset" value="Borrar">Borrar</button>
		</form>
	</main>
	
	<footer>
        <div class="columna">
          <h4>Sobre nosotros</h4>
          <p>El Festival de Diseño Gráfico es un evento anual que se celebra en distintas partes del mundo.</p>
        </div>
        <div class="columna">
          <h4>Contacto</h4>
          <p>Puedes contactarnos a través de nuestro correo electrónico: desing_cn@festivaledg.com</p>
        </div>
        <div class="columna">
          <h4>Síguenos en las redes sociales</h4>
          <ul>
            <li><a href="https://www.facebook.com/groups/graphicdesignerjobforall" target="_blank" rel="noopener noreferrer">Facebook</a></li>
            <li><a href="https://twitter.com/Adobe" target="_blank" rel="noopener noreferrer">Twitter</a></li>
            <li><a href="https://www.instagram.com/leots_digital/?hl=es" target="_blank" rel="noopener noreferrer">Instagram</a></li>
          </ul>
        </div>
      </footer>

      <script src="https://smtpjs.com/v3/smtp.js"></script>
      <script src="js/app.js"></script>
</body>
</html>

<?php
}else{
	 
    $cuerpo = "Nombre" . $_POST["nombre"] . "\r\n";
    $cuerpo = "Apellido: " . $_POST["apellido"] . "\r\n";
    $cuerpo .= "Email: " . $_POST["email"] . "\r\n";
	$cuerpo .= "Consulta: " . $_POST["mensaje"] . "\r\n";
    $cuerpo .= "Telefono: " . $_POST["tel"] . "\r\n";
	//las líneas de arriba definen el contenido del mail. Las palabras que están dentro de $_POST[""] deben coincidir con el "name" de cada campo del HTML. 
	// Si se agrega un campo al formulario, hay que agregarlo acá.

    $headers  = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/plain; charset=utf-8\n";
    $headers .= "X-Priority: 3\n";
    $headers .= "X-MSMail-Priority: Normal\n";
    $headers .= "X-Mailer: php\n";
    $headers .= "From: \"".$_POST['nombre']." ".$_POST['email']."\" <".$remitente.">\n";

    mail($destinatario, $asunto, $cuerpo, $headers);
    
    include 'confirma.html'; //se debe crear un html que confirma el envío
}
?>